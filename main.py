import pop

n_users = 80
n_files = 10

#reader = pop.DummyReader(n_users)
#reader = pop.SimpleMiningReader(n_users, n_files)
reader = pop.ProgressiveMiningReader(n_users, n_files)

#animation = pop.SimpleAnimation(reader, 200, 100)
animation = pop.MiningAnimation(reader, n_users)

animation.show()
