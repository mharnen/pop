import pop
import sys
from web3 import Web3

file_abi = './contract/pop.abi'
file_bin = './contract/pop.bin'
ipc_path = '/home/harnen/.ethereum/geth.ipc'
url = 'http://128.40.39.170:8080/'
contract_addr = '0xcC38b85D5db72DF4E9346d62Ec859fa7bAa38f7a'

if (len(sys.argv) != 2):
    print("Provide filename to register - python3 main_blockchain.py <contract_address>")
    quit()

contract_addr = Web3.toChecksumAddress(str(sys.argv[1]))

reader = pop.BlockchainReader(file_abi, file_bin, url, contract_addr)
print(reader._users)
animation = pop.BlockchainAnimation(reader)

animation.show()
