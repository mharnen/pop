pragma solidity ^0.5.1;

contract pop {
    mapping(address => uint) public balances;

    //initialized upon deployment
    address public owner = msg.sender;

    uint public creationTime = now;
    uint rewards = 0;
    uint registrationFee;
    mapping(string => bool) registeredFiles;

    //events
    event balanceChanged(address bidder, uint newAmount);
    event newTransfer(string fileId, address sender, address receiver);

    function put() public payable {
        balances[msg.sender] += msg.value;
        emit balanceChanged(msg.sender, balances[msg.sender]);
    }

    //check this for vulnerability
    function withdraw() public {
        uint amount = balances[msg.sender];
        balances[msg.sender] = 0;
        msg.sender.transfer(amount);
        emit balanceChanged(msg.sender, 0);
    }

    function putReward() public payable {
        rewards += msg.value;
    }
    
    function registerFile(string memory fileID) public payable {
        //we could check here, if the file is already registered
        if(msg.value > registrationFee){
            registeredFiles[fileID] = true;
        }
    }
    
    function registerTransfer(string memory fileID, address sender, address receiver) public {
        //if(registeredFiles[fileID]){
        emit newTransfer(fileID, sender, receiver);
        //}
    }
}

