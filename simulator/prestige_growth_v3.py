import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx
from functools import partial
# import mpld3

from simulator import *
from user import *

'''
def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy
'''

def getNodeSize():
    global users
    sizes = [user._prestige/size_divider for user in users.values()]
    return sizes

def getNodeColour():
    global users
    colours = []
    for user, staticValue in zip(users.values(), staticValues):
        if user._prestige <= 0.99 * staticValue:
            colours.append('green')
        else:
            colours.append('red')
    return colours

# Updates annotation per click
def update_annot(ind):
    user_index = ind["ind"][0]
    annot_pos = static_nodes.get_offsets()[user_index]
    annot.xy = annot_pos
    text = "User {}\nCois: {}\nDecay: {}\nStatic Value: {}\nPrestige: {}".format(user_index + 1, users[user_index]._coins, users[user_index]._d, labels_static[user_index], round(users[user_index]._prestige, 1))
    annot.set_text(text)

# On-click annotation control
def on_click(event):
    global ind, clicked
    if event.inaxes == ax:
        cont, ind = static_nodes.contains(event)
        if cont:
            clicked = True
            update_annot(ind)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            fig.canvas.draw_idle()

# on-press spacebar pause/unpause
def on_press(event):
    global pause
    if event.key.isspace():
        pause ^= True

    elif event.key == 'r':
        restart_animation()


def restart_animation():
    global prestige_nodes, users
    prestige_nodes.remove()
    users = {}
    users[0] = User(0, 80, 0, d=0.05)
    users[1] = User(1, 80, 0, d=0.15)
    users[2] = User(0, 50, 0, d=0.05)
    users[3] = User(1, 50, 0, d=0.15)
    prestige_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)



# Main animation update function
def updatefig(*args):
    global prestige_nodes, ind, pause
    if not pause:
        prestige_nodes.remove()
        prestige_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)
        
        if clicked:
            if len(ind["ind"]):
                user_index = ind["ind"][0]
                text = "User {}\nCois: {}\nDecay: {}\nStatic Value: {}\nPrestige: {}".format(user_index + 1, users[user_index]._coins, users[user_index]._d, labels_static[user_index], round(users[user_index]._prestige, 1))
                annot.set_text(text)

        for user in users.values():
            user.update()

############################################################################

# figure configurations
fig, ax = plt.subplots(figsize=(10,7))
fig.suptitle('The Growth of Prestige Over Time')
ax.set_xlim([0, 3])
ax.set_ylim([0, 3])
ax.axis('off')

# control frames before repeating
iterations = 10000

# control node size
size_divider = 0.1


# users = generateUsersCoinPrestige(n_nodes, 500, 0)
users = {}
users[0] = User(0, 80, 0, d=0.05)
users[1] = User(1, 80, 0, d=0.15)
users[2] = User(0, 50, 0, d=0.05)
users[3] = User(1, 50, 0, d=0.15)

# calculates static value for each user
staticValues = [user.staticValue() for user in users.values()]

# scales node size based on static value
staticSizes = [staticValue / size_divider for staticValue in staticValues]

# rounded off values
labels_static = [round(staticValue, 1) for staticValue in staticValues]

# DG = generateRandomTree(n_nodes)

# Graph initialising
G = nx.Graph()
G.add_nodes_from(users)

# controls position
#pos = nx.fruchterman_reingold_layout(G)
#pos = nx.spring_layout(DG, k=3)
#pos = nx.random_layout(G)
pos = {0: (0.5, 2.5), 1: (2.5, 2.5), 2: (0.5, 0.5), 3: (2.5, 0.5)}

# constant static value node size, no animation needed
static_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=staticSizes, node_color='grey')

# initialise prestige node
prestige_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)

# initialise annotation
## overheads
ind = None
clicked = False
pause = False

annot = ax.annotate("", xy=(0,0), xytext=(30,30),textcoords="offset points", fontsize='small', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"))
annot.set_visible(False)

# connects event to canvas
cid1 = fig.canvas.mpl_connect("button_press_event", on_click)
cid2 = fig.canvas.mpl_connect("key_press_event", on_press)

# animation
ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=20, repeat=True)

plt.show()

