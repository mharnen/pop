import sys
import copy
from random import expovariate, gauss, uniform
from user import *
from simulator import *


def printHelp():
    print("usage ./single <number_of_users> <number_of_iterations>")
    quit()




if(len(sys.argv) == 3):
    nUsers = int(sys.argv[1])
    nIterations = int(sys.argv[2])
    print("Generating ",  nUsers,  " users and running ",  nIterations, " iterations")
else:
    printHelp()




n_nodes = sys.argv[1]
iterations = int(sys.argv[2])


for i in range(0, iterations):
    #Generate random users with random numbers of base prestige
    users_orig = generateUsersPrestige(n_nodes, 100)
    #Copy the original users for simple and progressive mining
    users_simple = copy.deepcopy(users_orig)
    users_prog = copy.deepcopy(users_orig)
    DG = generateRandomTree(n_nodes)


    #perform simulation for simple mining
    calculatePrestigeSimple(users_simple, DG)
    generateStats(users_orig, users_simple, DG, 'simple', i)
    #perform simulation for progressive mining
    calculatePrestige(users_prog, DG)
    generateStats(users_orig, users_prog, DG, 'prog', i)

#print statistics
printStats()
