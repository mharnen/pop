import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx
from functools import partial
# import mpld3

from simulator import *
from user import *

def generateRandomTree(nodes):
    G = nx.random_tree(int(nodes))
    #convert the graph to a tree (unidirectional edges)
    return nx.bfs_tree(G,0).to_directed() # already directed, returns deep copy

def getNodeSize():
    global users
    sizes = [user._prestige/size_divider for user in users.values()]
    return sizes

def getNodeColour():
    global users
    colours = []
    for user, staticValue in zip(users.values(), staticValues):
        if user._prestige <= 0.9 * staticValue:
            colours.append('green')
        else:
            colours.append('red')
    return colours

#changed#
def update_annot(ind, plot):
    user_index = ind["ind"][0]
    annot_pos = plot.get_offsets()[user_index]
    annot.xy = annot_pos
    text = "User {}\nStatic Value: {}".format(user_index + 1, labels[user_index])
    annot.set_text(text)

#changed#
def on_click(event, plot):
    if event.inaxes == ax:
        cont, ind = plot.contains(event)
        if cont:
            # print(ind)
            update_annot(ind, plot)
            annot.set_visible(True)
            fig.canvas.draw_idle()
        else:
            annot.set_visible(False)
            fig.canvas.draw_idle()

def updatefig(*args):
    global prestige_nodes
    prestige_nodes.remove()
    prestige_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)
    
    for user in users.values():
        user.update()
    
    #changed#
    on_click_partial = partial(on_click, plot=static_nodes)
    cid = fig.canvas.mpl_connect("button_press_event", on_click_partial)

    ax.set_xlim([0, 3])
    ax.set_ylim([0, 3])
    ax.axis('off')

fig, ax = plt.subplots(figsize=(10,8))

# xn_nodes = 15
iterations = 10000

# control node size
size_divider = 0.1

# users = generateUsersCoinPrestige(n_nodes, 500, 0)
users = {}
users[0] = User(0, 80, 0, d=0.05)
users[1] = User(1, 80, 0, d=0.15)
users[2] = User(0, 50, 0, d=0.05)
users[3] = User(1, 50, 0, d=0.15)

staticValues = [user.staticValue() for user in users.values()]
staticSizes = [staticValue / size_divider for staticValue in staticValues]
labels = [round(staticValue, 1) for staticValue in staticValues]

# DG = generateRandomTree(n_nodes)
# Graph initialising
G = nx.Graph()
G.add_nodes_from(users)

#pos = nx.fruchterman_reingold_layout(G)
#pos = nx.spring_layout(DG, k=3)
#pos = nx.random_layout(G)
pos = {0: (2.5, 0.5), 1: (2.5, 2.5), 2: (0.5, 0.5), 3: (0.5, 2.5)}

annot = ax.annotate("", xy=(0,0), xytext=(30,30),textcoords="offset points", fontsize='small', fontweight='ultralight', bbox=dict(boxstyle="round", fc="w"))

annot.set_visible(False)

static_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=staticSizes, node_color='grey')
prestige_nodes = nx.draw_networkx_nodes(G, pos, with_labels=False, node_size=getNodeSize(), node_color=getNodeColour(), alpha=0.4)

ani = animation.FuncAnimation(fig, updatefig, frames=iterations, interval=300, repeat=True)

fig.suptitle('The Growth of Prestige Over Time')
plt.show()

