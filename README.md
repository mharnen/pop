# Proof of Prestige

This repository contains implementation of the [Proof of Prestige](http://sparta.ee.ucl.ac.uk/michal/pop-icbc19.pdf) protocol presented on [ICBC'19](http://icbc2019.ieee-icbc.org/). The `simulator` directory contains our (surprise surprise!) simulator implementation. Check a README file in the directory for instructions how to reproduce all the results included in the paper. 

The rest of the repository implements our demo described ([here](http://sparta.ee.ucl.ac.uk/michal/icbc_demo.pdf)) deployed on Ethereum blockchain. We include our Smart Contract serving as a gateway as well as as visualization moduler reading data from blockchain and displaying statistics as a matplotlib animation. Scripts included in the `scripts` directory allow to interact with our private ethereum blockchain instance, deploy the contract and introduce some users.

Furthermore, we've developed a mobile application for content dissemination registering file transfer on the blockchain available [here](https://play.google.com/store/apps/details?id=network.datahop.localsharing).




