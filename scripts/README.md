# Blockchain Script

The following scripts allow to interact with blockchain deployed at UCL server. You'll need  [web3py](https://github.com/ethereum/web3.py) to run them. 

There are 5 accounts to play with:

* Account #0: {36db55da1465d67830ef25e40687f3682da2f0a3}
* Account #1: {94e34fb2688f37fce3a8815947345335ba0503cd}
* Account #2: {95cd5339fa166f5e852c2514e6c261db8bcfecf5}
* Account #3: {60ea5e1cccc3e3e7e6eb65145759759cc4ea039d}
* Account #4: {ef436bc9f4eabbcfa90a6c882a3c010364ec4ccf}

The first one has almost infinite money, so use this one to send money to other account if needed. Money is need to invoke all the scripts that modify or deploy a contract. All the account have the same unlocking password `testtest`.

A typical use-case assumes invoking the scripts in the following order:

## deploy_contract.py
Compiles the contract located in ../contract/pop.sol and deploys it on blockchain. Each contract instance has its own variables, so playing with a new instance does not effect instances that are already deployed. Should be called only once. The script will print out an address of the newly deployed contract instance that should be used as an input to other scripts.

## deploy_money.py

Deploy moneys from the specified account into the specified contract. It injects the money into the Proof of Prestige scheme. 

## register_file.py
Takes as input a contract address and a filename to register: `python3 register_file.py <contract_address> <filename>`.

All the files must be registered before allowing transfers between users.

## register_transfer.py
Registers a transfer between a sender and recipient. Note that for correct completion the file must be registered before using `registr_file.py`


## send_money_to.py
Sends money from the main account (#0) to the selected account. Note that the money is expressed in wei so you need to send a lot. 

## get_events.py

Script for debugging. Connects to given contract and prints information about newly mined blocks and registered transfers. 


