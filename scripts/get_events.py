import json
import time
import web3
import threading
import sys


from web3 import Web3
from solc import compile_source
from web3.contract import ConciseContract



IPC_PATH='/home/harnen/.ethereum/geth.ipc'
URL='http://128.40.39.170:8080/'
CONTRACT_ADDR='0x29B471eA3057e459b04497ecf0E8Fa9bFC6162Cf'
poll_interval = 1 #in seconds
contract_file='../contract/pop.sol'
abi_file='../contract/pop.abi'
bin_file='../contract/pop.bin'
contract_abi = ""
contract_bin = ""

def generateStats(counter):
    global users,  users_coins, users_prestige

    for user_id in users:
        users_coins[user_id].append((counter, users[user_id].coins))
        users_prestige[user_id].append((counter, users[user_id].prestige))


if (len(sys.argv) != 2):
    print("Provide filename to register - python3 get_events.py <contract_address>")
    quit()

CONTRACT_ADDR = str(sys.argv[1])

with open(abi_file, 'r') as myfile:
    contract_abi = myfile.read()
    myfile.close()
with open(bin_file, 'r') as myfile:
    contract_bin = myfile.read()
    myfile.close()

# web3.py instance
#my_provider = Web3.IPCProvider(IPC_PATH)
my_provider = Web3.HTTPProvider(URL)

w3 = Web3(my_provider)
# set pre-funded account as sender
#w3.eth.defaultAccount = w3.eth.accounts[0]

print('Using contract address', CONTRACT_ADDR)

# Create the contract instance with the newly-deployed address
contract = w3.eth.contract(
    address=CONTRACT_ADDR,
    abi=contract_abi,
)

#event balanceChanged(address bidder, uint newAmount);
balance_event_filter = contract.events.balanceChanged.createFilter(fromBlock=0)
#event newTransfer(string fileId, address sender, address receiver);
transfer_event_filter = contract.events.newTransfer.createFilter(fromBlock=0)
block_event_filter = w3.eth.filter('latest')


while True:
        for event in balance_event_filter.get_new_entries():
            print("Balance of", event.args.bidder, "changed to", event.args.newAmount)

        for event in transfer_event_filter.get_new_entries():
            print("New transfer registered from ", event.args.sender, "to", event.args.receiver)

        for event in block_event_filter.get_new_entries():
            print("New block mined", event)


        time.sleep(poll_interval)
