import json
import web3
import sys

from web3 import Web3
from solc import compile_source
from web3.contract import ConciseContract


IPC_PATH='/home/harnen/.ethereum/geth.ipc'
CONTRACT_ADDR='0x29B471eA3057e459b04497ecf0E8Fa9bFC6162Cf'
URL='http://128.40.39.170:8080/'
contract_file='../contract/pop.sol'
abi_file='../contract/pop.abi'
bin_file='../contract/pop.bin'
contract_abi = ""
contract_bin = ""

if (len(sys.argv) != 3):
    print("Provide filename to register - python3 deploy_money.py <contract_address> <account_address>")
    quit()

CONTRACT_ADDR = str(sys.argv[1])
account_address = Web3.toChecksumAddress(sys.argv[2])
with open(abi_file, 'r') as myfile:
    contract_abi = myfile.read()
    myfile.close()
with open(bin_file, 'r') as myfile:
    contract_bin = myfile.read()
    myfile.close()

# web3.py instance
#my_provider = Web3.IPCProvider(IPC_PATH)
my_provider = Web3.HTTPProvider(URL)
w3 = Web3(my_provider)


# set pre-funded account as sender
w3.eth.defaultAccount = account_address

print("Account: ", account_address)


w3.personal.unlockAccount(w3.eth.defaultAccount, 'testtest')


print('Using contract address', CONTRACT_ADDR)

# Create the contract instance with the newly-deployed address
pop = w3.eth.contract(
    address=CONTRACT_ADDR,
    abi=contract_abi,
)
value = pop.functions.balances(account_address).call()
print("Initial balance: ", value)
# Display the default greeting from the contract
print("Sending 5 wei to the contract")
tx_hash = pop.functions.put().transact({'from': account_address, 'value': 50})

#print('Balances: {}'.format(
#    )
#))

#print('Setting the greeting to Nihao...')
#tx_hash = greeter.functions.setGreeting('Nihao').transact()

# Wait for transaction to be mined...
w3.eth.waitForTransactionReceipt(tx_hash)

value = pop.functions.balances(account_address).call()
print("Current balance: ", value)

# Display the new greeting value
#print('Updated contract greeting: {}'.format(
#    greeter.functions.greet().call()
#))

# When issuing a lot of reads, try this more concise reader:
#reader = ConciseContract(greeter)
#assert reader.greet() == "Nihao"
