import sys
from functools import partial
from itertools import product
from itertools import chain
from itertools import starmap
from matplotlib.ticker import MaxNLocator
import threading
import time
import abc
import copy

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx

from pop.user import *
from pop.reader import *
from pop.hierarchy_layout import *

class Animation(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def show(self):
        pass

class SimpleAnimation(Animation):

    def __init__(self, reader, iterations = 50, interval=100):
        self._iterations = iterations
        self._reader = reader
        reader.start()
        # figure configurations
        self._fig, self._ax= plt.subplots(2, sharex='none', figsize=(10, 7), gridspec_kw = {'height_ratios':[2, 1]})
        self._fig.suptitle('The Growth of Prestige Over Time')

        self._ax[1].set_xlim([0, 2.5])
        self._ax[1].set_ylim([0, 2.0])
        self._ax[1].axis('off')

        self._size_divider = 0.15
        # animation
        self._ani = animation.FuncAnimation(self._fig, self.updatefig, frames=iterations, interval=interval, repeat=True)

    def show(self):
        plt.show()


    # Main animation update function
    def updatefig(self, *args):
        self._ax[0].clear()

        users, users_coins, users_prestige = self._reader.getData()

        #print("Received users", users)
        #print("Received users_coins", users_coins)
        #print("Received users_prestige", users_prestige)

        for i in range(len(users)):
            print(users_prestige[list(users.keys())[i]])

        for user in users:
            keys = list([i[0] for i in users_prestige[user]])
            # i[0] is the first element of users_prestige[user] list
            # i.e. the counter number
            vals = list([i[1] for i in users_prestige[user]])
            # i[1] is the second element of users_prestige[user] list
            # i.e. the amount of prestige
            user_label = "C=" + str(users[user].coins) + ", d=" + str(users[user].d) # C = coins, d = decay parameter
            styles = ['dashed', 'dashdot', 'dashdot', 'dotted', 'solid']
            self._ax[0].plot(keys, vals, label=user_label, linewidth=2.2, linestyle=styles[list(users).index(user)])
            self._ax[0].plot((0, self._iterations), (users[user].staticValue(), users[user].staticValue()), 'k-', linestyle='dashed')
            self._ax[0].set_xlabel('Time[blocks]')
            self._ax[0].set_ylabel('Prestige')
            self._ax[0].legend()
            self._ax[0].set_xlim([0, self._iterations])
            self._ax[0].set_ylim([0, 500])

#Arnold

class MiningAnimation(Animation):

    def __init__(self, reader, interval=100):
        self._reader = reader
        reader.start()

        self._trees = self._reader.getTrees()
        self._users, _, _ = self._reader.getData()
        self._starting_static = self._reader.getStaticValues()

        self._overall_G = nx.DiGraph()
        self._overall_G.add_nodes_from(self._users)

        self._fig = plt.figure(figsize=(16, 7.5))
        self._fig.suptitle(self._reader._title)

        self._ax1 = self._fig.add_subplot(121)
        self._ax2 = self._fig.add_subplot(222)
        self._ax3 = self._fig.add_subplot(247)
        self._ax4 = self._fig.add_subplot(248)

        self._xlim = [0, 1]
        self._ylim = [-1, 0.1]

        self._annot_pos = [0, 0.3]
        self._file_annot_pos = [self._xlim[0], self._ylim[1] - 0.05]
        self._size_divider = 4

        self._miner = None  # initial definition of the miner

        self._pos = {}
        for file in self._trees:
            self._pos[file] = hierarchy_pos(self._trees[file], list(self._trees[file])[0])

        self._overall_pos = nx.spring_layout(self._overall_G, k=0.01, scale=0.45, center=(0.5, -0.5))

        '''
        self._labels = {}
        for user in list(self._G):
            self._labels[user] = user.id
        '''

        ###### overheads #####
        self._current_file = None
        self._ind = None
        self._current_address = None
        self._cont = False
        self._clicked = False

        self._cid1 = self._fig.canvas.mpl_connect("button_press_event", self.on_click)
        self._cid2 = self._fig.canvas.mpl_connect("key_press_event", self.on_press)

        self._ani = animation.FuncAnimation(self._fig, self.updatefig, interval=interval)

    def getStaticNodeSize(self, user_set):
        #staticValues = self._reader.getStaticValues()
        #return [staticValue / self._size_divider for staticValue in staticValues]

        staticValues = [self._users[address].staticValue() for address in user_set]
        return [staticValue / self._size_divider for staticValue in staticValues]

    def getPrestigeNodeSize(self, user_set):
        #return [self._users[address]._prestige / self._size_divider for address in self._users]

        return [self._users[address]._prestige / self._size_divider for address in user_set]

    def getNodeColour(self, file, miner, recently_traversed, traversed):
        colours = []

        # print('colour traversed nodes: {}'.format(traversed))
        if file is None:
            for node in list(self._overall_G):
                if self._current_address == node:
                    colours.append('orange')

                elif miner == node:
                   colours.append('yellow')

                elif self._users[node].prestige < 0.98 * self._users[node].staticValue():
                   colours.append('green')

                elif 0.98 * self._users[node].staticValue() <= self._users[node].prestige <= 1.02 * self._users[node].staticValue():
                   colours.append('blue')

                else:
                   colours.append('red')

        else:
            for node in list(self._trees[file]):
                # print(recently_traversed)
                if self._current_address == node:
                    colours.append('orange')

                elif miner == node:
                    colours.append('yellow')

                elif node in recently_traversed[file]:
                    colours.append('green')

                elif node in traversed[file]:
                    colours.append('cyan')

                else:
                    colours.append('None')
        # print('colours: {}'.format(colours))
        return colours

    def getLargeNodeColour(self, node):
        colours = []

        if node.prestige < 0.98 * node.staticValue():
            colours.append('green')
        elif 0.98 * node.staticValue() <= node.prestige <= 1.02 * node.staticValue():
            colours.append('blue')
        else:
            colours.append('red')

        return colours

    def on_click(self, event):
        if event.inaxes == self._ax1:
            self._cont, self._ind = self._static_nodes.contains(event)
            if self._cont:
                self._clicked = True

                user_index = self._ind["ind"][0]

                if self._current_file is None:
                    self._current_address = list(self._overall_G)[user_index]

                else:
                    self._current_address = list(self._trees[self._current_file])[user_index]

                self.update_annot(self._current_address)
                self.update_graph(self._current_address)
                self.update_node(self._current_address)

                self._annot.set_visible(True)
                self._fig.canvas.draw_idle()
            else:
                self._annot.set_visible(False)

                self._ax2.clear()
                self._ax3.clear()
                self._ax4.clear()

                self._ax2.axis('off')
                self._ax3.axis('off')
                self._ax4.axis('off')

                self._fig.canvas.draw_idle()

    def on_press(self, event):
        # self._clicked = False
        print("key pressed:", event.key)
        if event.key == 'escape':
            print("escape pressed")
            self._current_file = None
            self._file_annot.set_visible(True)
            self._fig.canvas.draw_idle()

        elif event.key == 'tab':
            print("tab pressed")
            if self._current_file is None:
                self._current_file = list(self._trees.keys())[int('0')]
            else:
                current_file_index = list(self._trees.keys()).index(self._current_file)
                if current_file_index == len(self._trees) - 1:
                    current_file_index = -1

                self._current_file = list(self._trees.keys())[current_file_index + 1]

    ##### update functions (ax2, 3, 4)#####
    def update_annot(self, address):

        _, annot_mining_prob = self._reader.getMiningData()

        text = "Address: \n{}\n\nCoins: {}\nStatic Value: {}\nPrestige: {}\nMining Probability: {}%\nPrestige Retained: {}\nTotal Retained: {}".format(
            address,
            round(self._users[address]._annot_coins, 1),
            round(self._users[address].staticValue(), 1),
            round(self._users[address]._annot_prestige, 1),
            round(annot_mining_prob[list(self._overall_G).index(address)] * 100, 3),
            round(self._users[address]._annot_gain, 2),
            round(self._users[address]._annot_work_gain, 2))

        self._annot = self._ax4.annotate(text, xy=self._annot_pos, fontsize='medium', fontweight='ultralight',
                             bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)


    def update_graph(self, address):
        _, users_coins, users_prestige = self._reader.getData()

        self._ax2.clear()

        current_static = self._users[address].staticValue()

        keys = list([i[0] for i in users_prestige[address]])
        vals = list([i[1] for i in users_prestige[address]])

        self._ax2.plot(keys, vals, linewidth=1)

        self._ax2.axhline(y=current_static, color='k', linestyle='dashed',
                    linewidth=1)  # current static in black
        self._ax2.axhline(y=self._starting_static[list(self._overall_G).index(address)], color='r', linestyle='dashed', linewidth=1)  # initial static in red

        self._ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
        self._ax2.set_yticks(np.arange(0, np.around(current_static * 1.5, decimals=-2),
                                 100 if current_static * 1.5 < 1000 else 1000 if current_static * 1.5 > 5000 else 500))

        self._ax2.grid(b=True)
        self._ax2.set_xlabel('Time[blocks]')
        self._ax2.set_ylabel('Prestige')

        self._ax2.set_ylim([0, current_static * 1.5])

    def update_node(self, address):

        self._ax3.clear()
        self._ax3.axis('off')

        largeNode = self._users[address]

        singleNode = nx.Graph()
        singleNode.add_node(largeNode)

        largeStaticSize = [largeNode.staticValue() * 4]
        largePrestigeSize = [largeNode.prestige * 4]

        self._largeStaticNode = nx.draw_networkx_nodes(singleNode, pos={largeNode: (1, 1)}, node_size=largeStaticSize, node_color='w',
                                                 edgecolors='k', linewidths=0.5, alpha=0.5, ax=self._ax3)
        self._largePrestigeNode = nx.draw_networkx_nodes(singleNode, pos={largeNode: (1, 1)}, node_size=largePrestigeSize,
                                                   node_color=self.getLargeNodeColour(largeNode), alpha=0.4, ax=self._ax3)


    def updatefig(self, *args):

        if self._reader.getConfirmed():
            users, _, _ = self._reader.getData()
            edgelist, recently_traversed, edgeLabelList, traversed = self._reader.getNetworkData()
            miner, _ = self._reader.getMiningData()

            self._ax1.clear()
            self._ax2.clear()
            self._ax3.clear()
            self._ax4.clear()

            self._ax1.set_xlim(self._xlim)
            self._ax1.set_ylim(self._ylim)
            self._ax1.axis('off')
            self._ax2.axis('off')
            self._ax3.axis('off')
            self._ax4.axis('off')

            if self._current_file is None:
                self._static_nodes = nx.draw_networkx_nodes(self._overall_G, self._overall_pos, node_size=self.getStaticNodeSize(list(self._overall_G)),
                                                            node_color='w', edgecolors='k',
                                                            linewidths=0.5, alpha=0.5, ax=self._ax1)

                self._prestige_nodes = nx.draw_networkx_nodes(self._overall_G, self._overall_pos, node_size=self.getPrestigeNodeSize(list(self._overall_G)),
                                                              node_color=self.getNodeColour(None, miner, recently_traversed,
                                                                                            traversed),
                                                              alpha=0.4, ax=self._ax1)
            else:
                self._static_nodes = nx.draw_networkx_nodes(self._trees[self._current_file], self._pos[self._current_file],
                                                            node_size=self.getStaticNodeSize(list(self._trees[self._current_file])),
                                                            node_color='w', edgecolors='k',
                                                            linewidths=0.5, alpha=0.5, ax=self._ax1)
                self._prestige_nodes = nx.draw_networkx_nodes(self._trees[self._current_file], self._pos[self._current_file],
                                                              node_size=self.getPrestigeNodeSize(list(self._trees[self._current_file])),
                                                              node_color=self.getNodeColour(self._current_file, miner, recently_traversed, traversed),
                                                              alpha=0.4, ax=self._ax1)
                #self._node_labels = nx.draw_networkx_labels(self._G, self._pos, font_size=9, ax=self._ax1)

                self._static_edges = nx.draw_networkx_edges(self._trees[self._current_file],
                                                            self._pos[self._current_file], alpha=0.1, ax=self._ax1)

                self._edges = nx.draw_networkx_edges(self._trees[self._current_file],
                                                     self._pos[self._current_file],
                                                     edgelist=edgelist[self._current_file], ax=self._ax1)

                self._edge_labels = nx.draw_networkx_edge_labels(self._trees[self._current_file],
                                                                 self._pos[self._current_file],
                                                                 edge_labels=edgeLabelList[self._current_file],
                                                                 font_size=8, ax=self._ax1)

                self._file_annot = self._ax1.annotate("{}".format(self._current_file), xy=self._file_annot_pos,
                                                fontsize='xx-large', fontweight='medium',
                                                bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)

            for user in users.values():
                user.annot_coins = user.coins
                user.annot_prestige = user.prestige
                user.annot_gain = user.gained
                user._annot_work_gain = user.work_gain
                user.gained = 0

            if self._clicked:
                if len(self._ind["ind"]):
                    self.update_annot(self._current_address)
                    self.update_graph(self._current_address)
                    self.update_node(self._current_address)

        else:
            print('reader not confirmed')

    def show(self):
        plt.show()


class BlockchainAnimation(Animation):

    def __init__(self, reader, interval=100):
        self._reader = reader
        reader.start()

        self._trees = self._reader.getTrees()
        self._users, _, _ = self._reader.getData()

        # self._starting_static = self._reader.getStaticValues()

        self._overall_G = nx.DiGraph()
        self._overall_G.add_nodes_from(self._users)

        self._fig = plt.figure(figsize=(16, 7.5))
        self._fig.suptitle('Blockchain Reader')

        self._ax1 = self._fig.add_subplot(121)
        self._ax2 = self._fig.add_subplot(222)
        self._ax3 = self._fig.add_subplot(247)
        self._ax4 = self._fig.add_subplot(248)

        self._xlim = [0, 1]
        self._ylim = [-1, 0.1]

        self._annot_pos = [0, 0.3]
        self._file_annot_pos = [self._xlim[0], self._ylim[1] - 0.05]
        self._size_divider = 0.2

        self._miner = None  # initial definition of the miner

        self._pos = {}
        for file in self._trees:
            self._pos[file] = hierarchy_pos(self._trees[file], list(self._trees[file])[0])

        self._overall_pos = nx.spring_layout(self._overall_G, k=0.01, scale=0.25, center=(0.5, -0.5))

        '''
        self._labels = {}
        for user in list(self._G):
            self._labels[user] = user.id
        '''

        ###### overheads #####
        self._current_file = None
        self._ind = None
        self._current_address = None
        self._cont = False
        self._clicked = False

        self._cid1 = self._fig.canvas.mpl_connect("button_press_event", self.on_click)
        self._cid2 = self._fig.canvas.mpl_connect("key_press_event", self.on_press)

        self._ani = animation.FuncAnimation(self._fig, self.updatefig, interval=interval)

    def getStaticNodeSize(self, user_set):

        staticValues = [self._users[address].staticValue() for address in user_set]
        return [staticValue / self._size_divider for staticValue in staticValues]

    def getPrestigeNodeSize(self, user_set):

        return [self._users[address]._prestige / self._size_divider for address in user_set]

    def getNodeColour(self, file, miner, recently_traversed, traversed):
        colours = []

        # print('colour traversed nodes: {}'.format(traversed))
        if file is None:
            for node in list(self._overall_G):
                if self._current_address == node:
                    colours.append('orange')

                elif miner == node:
                   colours.append('yellow')

                elif self._users[node].prestige < 0.98 * self._users[node].staticValue():
                   colours.append('green')

                elif 0.98 * self._users[node].staticValue() <= self._users[node].prestige <= 1.02 * self._users[node].staticValue():
                   colours.append('blue')

                else:
                   colours.append('red')

        else:
            for node in list(self._trees[file]):
                # print(recently_traversed)
                if self._current_address == node:
                    colours.append('orange')

                elif miner == node:
                    colours.append('yellow')

                #elif node in recently_traversed[file]:
                    #colours.append('green')

                elif node in traversed[file]:
                    colours.append('cyan')

                else:
                    colours.append('None')
        # print('colours: {}'.format(colours))
        return colours

    def getLargeNodeColour(self, node):
        colours = []

        if node.prestige < 0.98 * node.staticValue():
            colours.append('green')
        elif 0.98 * node.staticValue() <= node.prestige <= 1.02 * node.staticValue():
            colours.append('blue')
        else:
            colours.append('red')

        return colours

    def on_click(self, event):
        if event.inaxes == self._ax1:
            self._cont, self._ind = self._static_nodes.contains(event)
            if self._cont:
                self._clicked = True

                user_index = self._ind["ind"][0]

                if self._current_file is None:
                    self._current_address = list(self._overall_G)[user_index]

                else:
                    self._current_address = list(self._trees[self._current_file])[user_index]

                self.update_annot(self._current_address)
                self.update_graph(self._current_address)
                self.update_node(self._current_address)

                self._annot.set_visible(True)
                self._fig.canvas.draw_idle()
            else:
                self._annot.set_visible(False)

                self._ax2.clear()
                self._ax3.clear()
                self._ax4.clear()

                self._ax2.axis('off')
                self._ax3.axis('off')
                self._ax4.axis('off')

                self._fig.canvas.draw_idle()

    def on_press(self, event):
        # self._clicked = False
        print("key pressed:", event.key)
        if event.key == 'escape':
            print("escape pressed")
            self._current_file = None
            self._file_annot.set_visible(True)
            self._fig.canvas.draw_idle()

        elif event.key == 'n':
            print("n pressed")
            if self._current_file is None:
                self._current_file = list(self._trees.keys())[int('0')]
            else:
                current_file_index = list(self._trees.keys()).index(self._current_file)
                if current_file_index == len(self._trees) - 1:
                    current_file_index = -1

                self._current_file = list(self._trees.keys())[current_file_index + 1]

    ##### update functions (ax2, 3, 4)#####
    def update_annot(self, address):

        #_, annot_mining_prob = self._reader.getMiningData()

        text = "Address: \n{}\n\nCoins: {}\nStatic Value: {}\nPrestige: {}\nPrestige Retained: {}\nTotal Retained: {}".format(
            address,
            round(self._users[address]._annot_coins, 1),
            round(self._users[address].staticValue(), 1),
            round(self._users[address]._annot_prestige, 1),
            #round(annot_mining_prob[list(self._overall_G).index(address)] * 100, 3),
            round(self._users[address]._annot_gain, 2),
            round(self._users[address]._annot_work_gain, 2))

        self._annot = self._ax4.annotate(text, xy=self._annot_pos, fontsize='medium', fontweight='ultralight',
                             bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)


    def update_graph(self, address):
        _, users_coins, users_prestige = self._reader.getData()

        self._ax2.clear()

        current_static = self._users[address].staticValue()

        keys = list([i[0] for i in users_prestige[address]])
        vals = list([i[1] for i in users_prestige[address]])

        self._ax2.plot(keys, vals, linewidth=1)

        self._ax2.axhline(y=current_static, color='k', linestyle='dashed',
                    linewidth=1)  # current static in black
        #self._ax2.axhline(y=self._starting_static[list(self._overall_G).index(address)], color='r', linestyle='dashed', linewidth=1)  # initial static in red

        self._ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
        self._ax2.set_yticks(np.arange(0, np.around(current_static * 1.5, decimals=-2),
                                 100 if current_static * 1.5 < 1000 else 1000 if current_static * 1.5 > 5000 else 500))

        self._ax2.grid(b=True)
        self._ax2.set_xlabel('Time[blocks]')
        self._ax2.set_ylabel('Prestige')

        self._ax2.set_ylim([0, current_static * 1.5])

    def update_node(self, address):

        self._ax3.clear()
        self._ax3.axis('off')

        largeNode = self._users[address]

        singleNode = nx.Graph()
        singleNode.add_node(largeNode)

        largeStaticSize = [largeNode.staticValue() * 15]
        largePrestigeSize = [largeNode.prestige * 15]

        self._largeStaticNode = nx.draw_networkx_nodes(singleNode, pos={largeNode: (1, 1)}, node_size=largeStaticSize, node_color='w',
                                                 edgecolors='k', linewidths=0.5, alpha=0.5, ax=self._ax3)
        self._largePrestigeNode = nx.draw_networkx_nodes(singleNode, pos={largeNode: (1, 1)}, node_size=largePrestigeSize,
                                                   node_color=self.getLargeNodeColour(largeNode), alpha=0.4, ax=self._ax3)


    def updatefig(self, *args):

        self._trees = self._reader.getTrees()

        print("Animation received trees:", self._trees)

        self._pos = {}
        for file in self._trees:
            self._pos[file] = hierarchy_pos(self._trees[file], list(self._trees[file])[0])


        users, _, _ = self._reader.getData()
        traversed = self._reader.getNetworkData()
        recently_traversed = []
        # miner, _ = self._reader.getMiningData()
        print("animation received users:")
        for user_id in users:
            print("Coins:", users[user_id].coins, "prestige:", users[user_id].prestige)

        self._overall_G.add_nodes_from(self._users)
        self._overall_pos = nx.spring_layout(self._overall_G, k=0.01, scale=0.25, center=(0.5, -0.5), seed=1)

        miner = None

        self._ax1.clear()
        self._ax2.clear()
        self._ax3.clear()
        self._ax4.clear()

        self._ax1.set_xlim(self._xlim)
        self._ax1.set_ylim(self._ylim)
        self._ax1.axis('off')
        self._ax2.axis('off')
        self._ax3.axis('off')
        self._ax4.axis('off')

        if self._current_file is None:
            self._static_nodes = nx.draw_networkx_nodes(self._overall_G, self._overall_pos, node_size=self.getStaticNodeSize(list(self._overall_G)),
                                                        node_color='w', edgecolors='k',
                                                        linewidths=0.5, alpha=0.5, ax=self._ax1)

            self._prestige_nodes = nx.draw_networkx_nodes(self._overall_G, self._overall_pos, node_size=self.getPrestigeNodeSize(list(self._overall_G)),
                                                          node_color=self.getNodeColour(None, miner, recently_traversed,
                                                                                        traversed),
                                                          alpha=0.4, ax=self._ax1)
        else:
            self._static_nodes = nx.draw_networkx_nodes(self._trees[self._current_file], self._pos[self._current_file],
                                                        node_size=self.getStaticNodeSize(list(self._trees[self._current_file])),
                                                        node_color='w', edgecolors='k',
                                                        linewidths=0.5, alpha=0.5, ax=self._ax1)
            self._prestige_nodes = nx.draw_networkx_nodes(self._trees[self._current_file], self._pos[self._current_file],
                                                          node_size=self.getPrestigeNodeSize(list(self._trees[self._current_file])),
                                                          node_color=self.getNodeColour(self._current_file, miner, recently_traversed, traversed),
                                                          alpha=0.4, ax=self._ax1)
            #self._node_labels = nx.draw_networkx_labels(self._G, self._pos, font_size=9, ax=self._ax1)

            self._static_edges = nx.draw_networkx_edges(self._trees[self._current_file],
                                                        self._pos[self._current_file], alpha=0.1, ax=self._ax1)

            #self._edges = nx.draw_networkx_edges(self._trees[self._current_file],
            #                                     self._pos[self._current_file],
            #                                     edgelist=edgelist[self._current_file], ax=self._ax1)

            #self._edge_labels = nx.draw_networkx_edge_labels(self._trees[self._current_file],
            #                                                 self._pos[self._current_file],
            #                                                 edge_labels=edgeLabelList[self._current_file],
            #                                                 font_size=8, ax=self._ax1)

            self._file_annot = self._ax1.annotate("{}".format(self._current_file), xy=self._file_annot_pos,
                                            fontsize='xx-large', fontweight='medium',
                                            bbox=dict(boxstyle="round", fc="w"), annotation_clip=False)

        for user in users.values():
            user.annot_coins = user.coins
            user.annot_prestige = user.prestige
            user.annot_gain = user.gained
            user._annot_work_gain = user.work_gain
            user.gained = 0

        if self._clicked:
            if len(self._ind["ind"]):
                self.update_annot(self._current_address)
                self.update_graph(self._current_address)
                self.update_node(self._current_address)


    def show(self):
        plt.show()
