import threading
import time
import random
import abc
import string
import copy
from functools import partial
from itertools import product
from itertools import chain
from itertools import starmap
from random import *
from typing import Dict, List, Any, Tuple

import networkx as nx
import numpy as np

import web3
import networkx as nx
from web3 import Web3

from pop.user import *


class Reader(threading.Thread, metaclass=abc.ABCMeta):

    #returning a touple users, users_coins, users_prestige
    @abc.abstractmethod
    def getData(self) -> ({}, {}, {}):
        pass

    @abc.abstractmethod
    def run(self):
        pass

    @abc.abstractmethod
    def getTrees(self) -> {}:
        pass

class DummyReader(Reader):

    def __init__(self, n_users=4, interval=0.1):
        self._users = {}
        self._users_coins = {}
        self._users_prestige = {}
        self._trees = {}
        self._interval = interval
        self._counter = 0

        threading.Thread.__init__(self)


        #generate users
        for i in range(0, n_users):
            allchar = string.ascii_letters + string.digits
            address = "".join(choice(allchar) for x in range(40))
            address = "0x"+address
            print("Address", address)
            self._users[address] = User(address, coins = random.randint(0,100), prestige = 0)
            self._users_coins[address] = list()
            self._users_prestige[address] = list()

        #generate some trees
        for i in range(0, 4):
            filename = "file" + str(i) + ".dat"
            G = nx.DiGraph()
            for user in self._users:
                G.add_node(user)
            #TODO add some random edges here

            self._trees[filename] = G


    def run(self):
        while(True):
            for user_id in self._users:
                print("Adding stuff")
                self._users[user_id].update()
                self._users_coins[user_id].append((self._counter, self._users[user_id].coins))
                self._users_prestige[user_id].append((self._counter, self._users[user_id].prestige))

                print("Generated users", self._users)
                print("Generated users_coins", self._users_coins)
                print("Generated users_prestige", self._users_prestige)
                print("Generated counter", self._counter)
            self._counter += 1
            time.sleep(self._interval)  # maybe move to outside of for loop so all users update together

    def getData(self):
        return self._users, self._users_coins, self._users_prestige

    def getTrees(self):
        return self._trees


class MiningReader(Reader):

    def __init__(self, n_users=4, n_files=3, f=200, block_reward=50, interval=2):

        self._confirmed = False

        self._n_users = n_users

        self._users = {}
        self._users_coins = {}
        self._users_prestige = {}

        self._trees = {}
        self._user_sublists = {}
        self._roots = {}
        self._paths = {}

        self._edgelist = {}
        self._recently_traversed = {}
        self._edgeLabelList = {}

        self._f = f
        self._block_reward = block_reward

        self._interval = interval
        self._counter = 0

        self._miner = None

        threading.Thread.__init__(self)

        self._addressList = ["0x" + ("%064x" % random.randrange(10**64))[-40:] for _ in range(0, self._n_users)]
        for address in self._addressList:
            print("{}\n".format(address))

        for address in self._addressList:
            self._users[address] = User(address, uniform(20, 500), uniform(0, 0), work_prob=20)
            self._users_coins[address] = list()
            self._users_prestige[address] = list()


        # generate some trees
        for i in range(0, n_files):
            filename = "file" + str(i) + ".dat"
            G, users = self.generateRandomUserTree(random.randint(2, self._n_users))
            # TODO add some random edges here

            self._trees[filename] = G
            self._user_sublists[filename] = users
            self._roots[filename], self._paths[filename] = self.find_paths(G)
            self._traversed = copy.copy(self._roots)
            self._root0 = copy.deepcopy(self._roots)

            self._edgelist[filename] = []
            self._recently_traversed[filename] = []
            self._edgeLabelList[filename] = {}

        print(self._roots)
        print(self._traversed)

    def getStaticValues(self):
        return [self._users[address].staticValue() for address in self._addressList]

    def getData(self):
        return self._users, self._users_coins, self._users_prestige

    def getNetworkData(self):
        return self._edgelist, self._recently_traversed, self._edgeLabelList, self._traversed

    def getTrees(self):
        return self._trees

    def getConfirmed(self):
        return self._confirmed

    def getMiningData(self):
        return self._miner, self.get_mining_prob()

    def generateRandomTree(self, num_nodes):
        G = nx.random_tree(int(num_nodes))
        # convert the graph to a tree (unidirectional edges)
        return nx.bfs_tree(G, 0).to_directed()  # already directed, returns deep copy

    def userDirectedGraph(self, G):
        return [(self._users[edge[0]], self._users[edge[1]]) for edge in G.edges()]

    def generateRandomUserTree(self, num_nodes):
        # Graph with empty nodes and directed edges
        G = nx.random_tree(int(num_nodes))
        G = nx.bfs_tree(G, 0).to_directed()
        # Create directed graph with users as nodes by copying edges from G to DG
        thisUserList = np.random.choice(self._addressList, num_nodes, replace=False).tolist()
        print(thisUserList)
        DG = nx.DiGraph()
        DG.add_nodes_from(thisUserList)
        DG.add_edges_from([(thisUserList[edge[0]], thisUserList[edge[1]]) for edge in G.edges()]) # use address_list when needed
        return DG, thisUserList

    def find_paths(self, G):
        # get roots and leaves to find all the paths in the tree we have to traverse
        chaini = chain.from_iterable
        roots = [v for v, d in G.in_degree() if d == 0]
        leaves = (v for v, d in G.out_degree() if d == 0)
        all_paths = partial(nx.all_simple_paths, G)
        found_paths = list(chaini(starmap(all_paths, product(roots, leaves))))
        return roots, found_paths

    ##### Mining Probability #####
    def get_mining_prob(self):
        #global DG, users, total_prestige
        total_prestige = sum(user.prestige for user in self._users.values())
        if total_prestige == 0:
            return [1 / self._n_users for _ in self._addressList]
        else:
            return [self._users[address].prestige / total_prestige for address in self._addressList]


class SimpleMiningReader(MiningReader):

    def __init__(self, *args):
        super().__init__(*args)
        self._title = 'Simple Mining Simulation'

    def run(self):
        while True:

            self._confirmed = False

            print('----------------------------------------------------------------------------------------------------------------------------')
            print('Block: {}'.format(self._counter))

            for file in self._trees:

                new_transfers_edges = []
                new_transfers_nodes = []
                new_transfers_amount = {}

                #print("traversed: {}".format(self._traversed))
                #print("DG: {}".format(self._DG.nodes()))

                root_gen = [root_node for root_node in self._roots[file] if
                            not all(x in self._traversed[file] for x in self._trees[file].successors(root_node))]
                #print(root_gen)
                for root_node in root_gen:
                    #print('root_node: {}'.format(root_node))
                    #print("successors:{}".format(list(self._DG.successors(root_node))[0].id))
                    successor_gen = [successor_node for successor_node in self._trees[file].successors(root_node) if
                                     successor_node not in self._traversed[file]]
                    #print(successor_gen)
                    for successor_node in successor_gen:
                        #print('successor_node: {}'.format(successor_node))

                        if self._users[successor_node].isWorking() and (self._users[successor_node].prestige + self._users[successor_node].gained >= self._f):

                            # prestige transfer
                            self._users[root_node].gained += self._f
                            self._users[successor_node].gained -= self._f

                            print("")
                            print('Node:   {} distributes {} to Node: {}'.format(root_node, file, successor_node))
                            print('Transaction:')
                            print('From:   {}'.format(successor_node))
                            print('To:     {}'.format(root_node))
                            print('amount: {}'.format(self._f))

                            #print((successor_node, root_node))
                            new_transfers_edges.append((successor_node, root_node))
                            new_transfers_nodes.append(successor_node)
                            new_transfers_amount[(successor_node, root_node)] = self._f

                            # print("new_transfers_nodes: {}".format(new_transfers_nodes))

                            self._edgelist[file] = copy.copy(new_transfers_edges)
                            self._recently_traversed[file] = copy.copy(new_transfers_nodes)
                            #print(self._recently_traversed)
                            self._edgeLabelList[file] = copy.copy(new_transfers_amount)

                            self._traversed[file].append(successor_node)
                            #print(self._traversed)

                self._roots = copy.copy(self._traversed)

            #block_id += 1

            for user in self._users.values():
                user.prestige += user.gained
                user.work_gain += user.gained

            for address in self._users:
                #print("Adding stuff")
                self._users[address].update()
                self._users_coins[address].append((self._counter, self._users[address].coins))
                self._users_prestige[address].append((self._counter, self._users[address].prestige))

                #print("Generated users", self._users)
                #print("Generated users_coins", self._users_coins)
                #print("Generated users_prestige", self._users_prestige)

            # print("Generated counter", self._counter)

            self._total_prestige = sum(user._prestige for user in
                                       self._users.values())  # initial calculation of total prestige for mining probability (0)

            self._miner = np.random.choice(self._addressList, p=self.get_mining_prob())
            self._users[self._miner].coins += self._block_reward

            self._counter += 1

            print('\nMiner: {}'.format(self._miner))
            print('----------------------------------------------------------------------------------------------------------------------------\n')
            self._confirmed = True
            time.sleep(self._interval)# maybe move to outside of for loop so all users update together


class ProgressiveMiningReader(MiningReader):

    def __init__(self, *args):
        super().__init__(*args)
        self._title = 'Progressive Mining Simulation'

    def run(self):
        while True:

            self._confirmed = False

            print('----------------------------------------------------------------------------------------------------------------------------')
            print('Block: {}'.format(self._counter))

            for file in self._trees:

                new_transfers_edges = []
                new_transfers_nodes = []
                new_transfers_amount = {}

                root_gen = (root_node for root_node in self._roots[file] if
                            not all(x in self._traversed[file] for x in self._trees[file].successors(root_node)))
                for root_node in root_gen:
                    # print('root_node: {}'.format(root_node))

                    successor_gen = (successor_node for successor_node in self._trees[file].successors(root_node) if
                                     successor_node not in self._traversed[file])
                    for successor_node in successor_gen:
                        # print('successor_node: {}'.format(successor_node))

                        if self._users[successor_node].isWorking() and (self._users[successor_node].prestige + self._users[successor_node].gained >= self._f):

                            print("")
                            print('Node:   {} distributes {} to Node: {}'.format(root_node, file, successor_node))
                            print('Transaction:')

                            ### new progressive section ###
                            current_node = successor_node
                            pred = list(self._trees[file].predecessors(current_node))
                            prestige_transit = 0

                            while pred:
                                #print(file)
                                #print(self._root0[file])
                                #print(pred)
                                #print(pred[0])
                                path_before = nx.shortest_path(self._trees[file], self._root0[file][0], pred[0])
                                path_strength = self.calculatePathStrengthSum(path_before)
                                node_strength = self.calculateNodeStrength(current_node)

                                info = self.animationCalculateTransfer(file, current_node, node_strength, path_strength,
                                                                  prestige_transit)
                                prestige_transit = info['transfer']
                                self._users[current_node].gained += info['gained'] - info['lost']
                                # users[current_node].prestige += users[current_node].gained

                                sn = current_node
                                rn = pred[0]
                                new_transfers_edges.append((sn, rn))
                                if (sn, rn) in new_transfers_amount:
                                    new_transfers_amount[(sn, rn)] += round(prestige_transit, 2)
                                else:
                                    new_transfers_amount[(sn, rn)] = round(prestige_transit, 2)

                                # print('{} transfers {} upstream'.format(sn, prestige_transit))

                                current_node = pred[0]
                                pred = list(self._trees[file].predecessors(current_node))

                            # print("Adding ", prestige_transit, "to root ", current_node)
                            self._users[current_node].gained += prestige_transit

                            # new_transfers_edges.append((successor_node, root_node))
                            new_transfers_nodes.append(successor_node)
                            # new_transfers_amount[(successor_node, root_node)] = misc.f

                            self._edgelist[file] = copy.copy(new_transfers_edges)
                            self._recently_traversed[file] = copy.copy(new_transfers_nodes)
                            self._edgeLabelList[file] = copy.copy(new_transfers_amount)

                            self._traversed[file].append(successor_node)
                            #print(self._traversed)

                self._roots = copy.copy(self._traversed)

            #block_id += 1

            for user in self._users.values():
                user.prestige += user.gained
                user.work_gain += user.gained

            for user_id in self._users:
                #print("Adding stuff")
                self._users[user_id].update()
                self._users_coins[user_id].append((self._counter, self._users[user_id].coins))
                self._users_prestige[user_id].append((self._counter, self._users[user_id].prestige))

                #print("Generated users", self._users)
                #print("Generated users_coins", self._users_coins)
                #print("Generated users_prestige", self._users_prestige)

            # print("Generated counter", self._counter)

            self._total_prestige = sum(user._prestige for user in
                                       self._users.values())  # initial calculation of total prestige for mining probability (0)

            self._miner = np.random.choice(self._addressList, p=self.get_mining_prob())
            self._users[self._miner].coins += self._block_reward

            self._counter += 1

            print('\nMiner: {}'.format(self._miner))
            print('----------------------------------------------------------------------------------------------------------------------------\n')
            self._confirmed = True
            time.sleep(self._interval)  # maybe move to outside of for loop so all users update together

    ################################################
    # calculate path strength using sum function   #
    ################################################
    def calculatePathStrengthSum(self, path):
        sum_prestige = 0
        for node in path:
            sum_prestige += self._users[node].prestige

        return sum_prestige

    #############################
    # calculate node strength   #
    #############################
    def calculateNodeStrength(self, node):
        return self._users[node].prestige * 2

    def animationCalculateTransfer(self, file, node, node_strength, path_strength, prestige_transit):
        # fee paid by the node
        # The node is added to traversed to pay only once
        if node not in self._traversed[file]:
            node_loses = self._f
        else:
            node_loses = 0

        # The retain function, user retains some value from downstream and the rest is sent upstream
        node_gains = (prestige_transit * node_strength) / (path_strength + node_strength)

        transfered_up = (prestige_transit + node_loses - node_gains)

        print('From:                        {}'.format(node))
        print('Amount transferred upstream: {}'.format(transfered_up))
        print('Retained:                    {}'.format(node_gains))

        return {'lost': node_loses, 'gained': node_gains, 'transfer': transfered_up}


class BlockchainReader(Reader):
    def __init__(self, file_abi, file_bin, url, contract_addr, interval = 1):
        self._users = {}
        self._users_coins = {}
        self._users_prestige = {}
        self._interval = interval
        self._counter = 0
        self._contract_abi = ""
        self._contract_bin = ""

        self._trees = {}

        self._traversed = {}

        self._edgelist = {}
        self._recently_traversed = {}
        self._edgeLabelList = {}

        threading.Thread.__init__(self)

        with open(file_abi, 'r') as myfile:
            self._contract_abi = myfile.read()
            myfile.close()
        with open(file_bin, 'r') as myfile:
            self._contract_bin =myfile.read()
            myfile.close()


        # web3.py instance
        #my_provider = Web3.IPCProvider(ipc_path)
        my_provider = Web3.HTTPProvider(url)
        self._w3 = Web3(my_provider)
        self._w3.eth.defaultAccount = self._w3.eth.accounts[0]

        print('Using contract address', contract_addr)

        # Create the contract instance with the newly-deployed address
        contract = self._w3.eth.contract(
            address=contract_addr,
            abi=self._contract_abi,
        )

        self._balance_event_filter = contract.events.balanceChanged.createFilter(fromBlock=0)
        self._transfer_event_filter = contract.events.newTransfer.createFilter(fromBlock=0)
        self._block_event_filter = self._w3.eth.filter('latest')

        '''
        for account in self._w3.eth.accounts:
        #    print(account)
            value = contract.functions.balances(account).call()
            self._users[account] = User(account, coins=value, prestige=0)
            self._users_coins[account] = list()
            self._users_prestige[account] = list()
        #    print(value)
        '''


    def run(self):


            # self._users[account] = User(account, coins=event.args.newAmount, prestige=0)

        while True:
            print(self._counter)
            for event in self._balance_event_filter.get_new_entries():
                print("***************")
                # print(event)
                print("Balance of", event.args.bidder, "changed to", event.args.newAmount)
                #create this user in our data base if not present
                if event.args.bidder not in self._users:
                    print("User with ID", event.args.bidder, "is not present in our database. Creating with initial money", event.args.newAmount)
                    self._users[event.args.bidder] = User(event.args.bidder, coins = event.args.newAmount, prestige = 0)
                    self._users_coins[event.args.bidder] = list()
                    self._users_prestige[event.args.bidder] = list()
                else:
                    print("User with ID", event.args.bidder, "already in our database. Setting money to", event.args.newAmount)
                    self._users[event.args.bidder].coins = event.args.newAmount

            for event in self._transfer_event_filter.get_new_entries():
                filename = event.args.fileId
                print(filename)
                print("New transfer from", event.args.sender, "to", event.args.receiver, "file ID: ", filename)
                self._users[event.args.sender].prestige += 50
                self._users[event.args.receiver].prestige -= 50

                if filename not in self._trees:
                    G = nx.DiGraph()
                    G.add_nodes_from([event.args.sender, event.args.receiver])
                    G.add_edge(event.args.sender, event.args.receiver)
                    self._trees[filename] = G
                    self._traversed[filename] = list()
                    self._traversed[filename].append(event.args.sender)
                    self._traversed[filename].append(event.args.receiver)


                else:
                    self._trees[filename].add_edge(event.args.sender, event.args.receiver)
                    self._traversed[filename].append(event.args.receiver)


                '''
                # generate some trees
                for i in range(0, n_files):
                    filename = "file" + str(i) + ".dat"
                    G, users = self.generateRandomUserTree(random.randint(2, self._n_users))
                    # TODO add some random edges here

                    self._trees[filename] = G
                    self._user_sublists[filename] = users
                    self._roots[filename], self._paths[filename] = self.find_paths(G)
                    self._traversed = copy.copy(self._roots)
                    self._root0 = copy.deepcopy(self._roots)

                    self._edgelist[filename] = []
                    self._recently_traversed[filename] = []
                    self._edgeLabelList[filename] = {}
                '''




            for event in self._block_event_filter.get_new_entries():
                print("New block mined", event)
                for user_id in self._users:
                    self._users[user_id].update()
                    self._users_coins[user_id].append((self._counter, self._users[user_id].coins))
                    self._users_prestige[user_id].append((self._counter, self._users[user_id].prestige))
                    print("User", user_id, "current prestige", self._users[user_id].prestige)
                self._counter += 1
            time.sleep(self._interval)

    def getData(self):
        return self._users, self._users_coins, self._users_prestige

    def getTrees(self):
        return self._trees

    def getNetworkData(self):
        return self._traversed
