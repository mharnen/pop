# Blockchain Reader and Visualization module for Proof of Prestige

The system implements different readers acquiring data from blockchain (or dummy traces). The acquired data can be then pulled by the reader and displayed as a matplotlib simulation

## Instalation

TODO

## Running
`python3 main_blockchain.py`

## Simple Mining Animation
The new simple mining animation will now read from an external reader
It will need the following inputs to function
1. A list of users of class `User` (see user.py for class `User`)
2. `user_coins` and `user_prestige` are lists of all historical coins and prestige information
    * {  
      [(block_0, user1.prestige),(block_1, user1.prestige)...  ]
      [(block_0, user2.prestige),(block_1, user2.prestige)...  ]
      ...  
      }
3. `G` a directed graph, showing the transfer paths (may not be necessary for real blcokchain implemention)
4. `staticValues` a list of all static values for each user in the network
5. `miner` an integer, a user index of the miner
6. `mining_prob` a list of all mining probability of each user in the network
7. `confirmed` a boolean value that checks if the most recent block has been mined and notifies the animation to update

